*** Settings ***
Documentation    Direct and reverse geocoding tests.
Library          entrycase.py

*** Test Cases ***      ADDRESS  ->  LATITUDE  ->  LONGITUDE
Test 1
    [Documentation]     Direct geocoding test
    [Template]          direct test
						"15, 4-я Лариновская улица, Ranica, Orsha, Orsha District, Vitsebsk Region, 211387, Belarus"  54.50895215  30.4426101420229
                        "152, 44th Street, Sunset Park, Brooklyn, Kings County, New York, 11232, USA"  40.65277985  -74.014599515367
						"University Drive, Ooralea, Mackay, Queensland, 4740, Australia"  -21.1753214  149.1432747
						"166, Невский проспект, Peski, округ Смольнинское, Центральный район, Saint Petersburg, Northwestern Federal District, 190000, Russia"  59.9265911  30.377558
						"Метро «Кропоткинская», Gogolevskiy Blvd, Старое Ваганьково, Moscow, Central Administrative Okrug, Moscow, Central Federal District, 119019, Russia"  55.7448351  37.6009851
						"Airport Pulkovo, Взлётная улица, Aviagorodok, округ Пулковский меридиан, Московский район, Saint Petersburg, Northwestern Federal District, 196210, Russia"  59.7997105  30.273511
						"118А, улица Бакунина, Западный, 59-й микрорайон, Советский район, Homel, Homel Region, 246044, Belarus"  52.42402745  30.9659212701613
				
Test 2
    [Documentation]     Reverse geocoding test
    [Template]          reverse test
						городской округ Шатура, Московская область, ЦФО, Россия  55.4231244  39.8766002426334
						39, Советская улица, Кронштадт, Кронштадтский район, Санкт-Петербург, Северо-Западный федеральный округ, 190000, РФ  59.99453265  29.7751669034231
						Nevskiy.166, 166, Невский проспект, Пески, округ Смольнинское, Центральный район, Санкт-Петербург, Северо-Западный федеральный округ, 190000, РФ  59.9265911  30.377558
						20, Обская улица, Советский район, Уфа, городской округ Уфа, Башкортостан, Приволжский федеральный округ, 450000, РФ  54.7181769  55.997780569907
						118А, улица Бакунина, Западный, 59-й микрорайон, Советский район, Гомель, Гомельская область, 246044, Беларусь  52.42402745  30.9659212701613
						15, 4-я Лариновская улица, Раница, Орша, Оршанский район, Витебская область, 211387, Беларусь  54.50895215  30.4426101420229
						3, Гоголевский бульвар, Старое Ваганьково, Москва, Центральный административный округ, Москва, ЦФО, 119019, Россия  55.7448351  37.6009851