import requests
import xml.dom.minidom
import unittest

class entrycase(unittest.TestCase):
	
	def direct_test(self, address, latitude, longitude):
		query = {'q': address, 'format': 'xml'} 											#формирование запроса
		req = requests.get('https://nominatim.openstreetmap.org/search', query) 			#отправление запроса
		parse = xml.dom.minidom.parseString(req.text) 										#парсинг строки в xml формате
		node = parse.getElementsByTagName("place") 											#нахождение элемента по тэгу "place"
		coordinates = node[0].getAttribute("lat") + ' ' + node[0].getAttribute("lon") 		#координаты с сервера
		coordinatesDB = latitude + ' ' + longitude 											#координаты из базы(допустим, из какого-то справочника)
		self.assertEqual(coordinates, coordinatesDB) 										#сравнение координат с сервера и из базы
	
	def reverse_test(self, address, latitude, longitude):
		query = {'lat': latitude, 'lon': longitude, 'format': 'xml'}						#формирование запроса
		req = requests.get('https://nominatim.openstreetmap.org/reverse', query)			#отправление запроса
		parse = xml.dom.minidom.parseString(req.text)										#парсинг строки в xml формате
		node = parse.getElementsByTagName("result")											#нахождение элемента по тэгу "result"
		addressMap = node[0].childNodes[0].nodeValue										#адрес с сервера
		addressDB = address																	#адрес из базы(допустим, из какого-то справочника)
		self.assertEqual(addressMap, addressDB)												#сравнение адресов с сервера и из базы